import logging.config
import os

import yaml


def setup_logging(path):
    """Setup logging with logging module.

    Args:
        path (str): Path to logging yaml config.
    """
    if os.path.exists(path):
        with open(path, "rt") as f:
            try:
                config = yaml.safe_load(f.read())
                logging.config.dictConfig(config)
            except Exception:
                print("Error in Logging Configuration")
                raise
    else:
        raise OSError("Failed to load logging configuration file")
